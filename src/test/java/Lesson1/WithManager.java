package Lesson1;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class WithManager {
    @Test
    public void test(){
        WebDriverManager.chromedriver().setup();        //Запуск хром драйвера
        WebDriver driver = new ChromeDriver();          //Запуск переменной driver в хроме
        WebDriver user = new ChromeDriver();
        WebDriver admin = new ChromeDriver();
        driver.quit();                                  //в конце теста мы закрываем окно хрома - правила хорошего тона
        user.quit();
    }
}
